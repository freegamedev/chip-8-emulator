﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Chip8
{
    public delegate void delKeyDown(Keys key);
    public delegate void delKeyUp(Keys key);

    class KeyboardHook:IDisposable
    {
        #region Flags
        [Flags]
        public enum KeyboardEvents
        {
            ExtendedKey = 1,
            KeyUp = 2,
            ScanCode = 8,
            Unicode = 4
        }
        #endregion
        #region Enum
        public enum KeyboardWindowMessage
        {
            Activate = 6,
            ApplicationCommand = 0x319,
            Character = 0x102,
            DeadCharacter = 0x103,
            Hotkey = 0x312,
            KeyDown = 0x100,
            KeyUp = 0x101,
            KillFocus = 8,
            None = 0,
            SetFocus = 7,
            SystemDeadCharacter = 0x107,
            SystemKeyDown = 260,
            SystemKeyUp = 0x105,
            UnicodeCharacter = 0x109
        }
        #endregion
        #region Struct
        [StructLayout(LayoutKind.Sequential)]
        public struct KeyboardInput
        {
            public short VirtualKey;
            public short ScanCode;
            public KeyboardEvents KeyboardEvent;
            public int Time;
            private IntPtr extraInfo;
        }
        #endregion
        #region Deligates       
        public event delKeyDown KeyDown;
        public event delKeyUp KeyUp;
        #endregion
        #region HookProc
        NativeMethods.HookProc KeyHookProcedure;
        #endregion
        #region Variables
        /// <summary>Pointer to keyboard hook</summary>
        IntPtr kHook = IntPtr.Zero;
        /// <summary>Pointer to library instance</summary>
        IntPtr hInstance;
        /// <summary>Virtual Key to end hooking</summary>
        public Keys UnHookKey = Keys.Escape;
        /// <summary>Flag indicating if keyboard is hooked</summary>
        public bool IsHooked = false;
        /// <summary>Keeps track of keystates for all keys</summary>
        bool[] keyState = new bool[512];
        #endregion
        #region Cleanup
        ~KeyboardHook()
        {
            if (IsHooked)
            {
                UnHookEvents();
            }
        }
        public void Dispose()
        {
            if (IsHooked)
            {
                UnHookEvents();
            }
        }
        #endregion
        /// <summary>Converts virtual key to System.Windows.Forms.Keys</summary>
        /// <param name="virtualKey">vk code to convert</param>
        /// <returns>System.Windows.Forms.Keys</returns>
        Keys HookVK2Keys(int virtualKey)
        {
            switch (virtualKey)
            {
                case 8:
                    return Keys.Back;

                case 9:
                    return Keys.Tab;

                case 13:
                    return Keys.Enter;

                case 20:
                    return Keys.CapsLock;

                case 0x1b:
                    return Keys.Escape;

                case 0x20:
                    return Keys.Space;

                case 0x25:
                    return Keys.Left;

                case 0x26:
                    return Keys.Up;

                case 0x27:
                    return Keys.Right;

                case 40:
                    return Keys.Down;

                case 0x2e:
                    return Keys.Delete;

                case 0x30:
                    return Keys.D0;

                case 0x31:
                    return Keys.D1;

                case 50:
                    return Keys.D2;

                case 0x33:
                    return Keys.D3;

                case 0x34:
                    return Keys.D4;

                case 0x35:
                    return Keys.D5;

                case 0x36:
                    return Keys.D6;

                case 0x37:
                    return Keys.D7;

                case 0x38:
                    return Keys.D8;

                case 0x39:
                    return Keys.D9;

                case 0x41:
                    return Keys.A;

                case 0x42:
                    return Keys.B;

                case 0x43:
                    return Keys.C;

                case 0x44:
                    return Keys.D;

                case 0x45:
                    return Keys.E;

                case 70:
                    return Keys.F;

                case 0x47:
                    return Keys.G;

                case 0x48:
                    return Keys.H;

                case 0x49:
                    return Keys.I;

                case 0x4a:
                    return Keys.J;

                case 0x4b:
                    return Keys.K;

                case 0x4c:
                    return Keys.L;

                case 0x4d:
                    return Keys.M;

                case 0x4e:
                    return Keys.N;

                case 0x4f:
                    return Keys.O;

                case 80:
                    return Keys.P;

                case 0x51:
                    return Keys.Q;

                case 0x52:
                    return Keys.R;

                case 0x53:
                    return Keys.S;

                case 0x54:
                    return Keys.T;

                case 0x55:
                    return Keys.U;

                case 0x56:
                    return Keys.V;

                case 0x57:
                    return Keys.W;

                case 0x58:
                    return Keys.X;

                case 0x59:
                    return Keys.Y;

                case 90:
                    return Keys.Z;

                case 0x5b:
                    return Keys.LWin;

                case 0x70:
                    return Keys.F1;

                case 0x71:
                    return Keys.F2;

                case 0x72:
                    return Keys.F3;

                case 0x73:
                    return Keys.F4;

                case 0x74:
                    return Keys.F5;

                case 0x75:
                    return Keys.F6;

                case 0x76:
                    return Keys.F7;

                case 0x77:
                    return Keys.F8;

                case 120:
                    return Keys.F9;

                case 0x79:
                    return Keys.F10;

                case 0x7a:
                    return Keys.F11;

                case 0x7b:
                    return Keys.F12;

                case 160:
                    return Keys.ShiftKey;

                case 0xa1:
                    return Keys.RShiftKey;

                case 0xa2:
                    return Keys.ControlKey;

                case 0xa4:
                    return Keys.Menu;

                case 0xa5:
                    return Keys.Menu;

                case 0xba:
                    return Keys.Oem1;

                case 0xbb:
                    return Keys.Oemplus;

                case 0xbc:
                    return Keys.Oemcomma;

                case 0xbd:
                    return Keys.OemMinus;

                case 190:
                    return Keys.OemPeriod;

                case 0xbf:
                    return Keys.Oem2;

                case 0xc0:
                    return Keys.Oemtilde;

                case 0xdb:
                    return Keys.Oem4;

                case 220:
                    return Keys.Oem5;

                case 0xdd:
                    return Keys.Oem6;

                case 0xde:
                    return Keys.Oem7;
            }
            return Keys.None;
        }
        public void HookEvents()
        {
            KeyHookProcedure = new NativeMethods.HookProc(KeyHookProc);

            hInstance = NativeMethods.LoadLibrary("User32");
            kHook = NativeMethods.SetWindowsHookEx(NativeMethods.HookType.LowLevelKeyboardHook, KeyHookProcedure, hInstance, 0);
            
            if (kHook == IntPtr.Zero)
            {
                //Console.WriteLine("HookEvents : Keyboard Hook Failed", "HookEvents Error");
                UnHookEvents();
                return;
            }
            IsHooked = true;
        }
        public void UnHookEvents()
        {
            try
            {
                NativeMethods.FreeLibrary(hInstance);
            }
            catch (Exception ex)
            {
                Console.WriteLine("UnHookEvents : " + ex.ToString(), "UnHookEvents Error"); 
            }
            finally
            {
                if (kHook != IntPtr.Zero)
                {
                    bool ret = NativeMethods.UnhookWindowsHookEx(kHook);
                    if (ret == false)
                    {
//                        Console.WriteLine("UnHookEvents : keyboard unhook failed", "UnHookEvents Error");
                    }
                }
                //If the UnhookWindowsHookEx function fails.
                kHook = IntPtr.Zero;
                IsHooked = false;
            }
        }
        [System.Security.Permissions.PermissionSetAttribute(System.Security.Permissions.SecurityAction.LinkDemand, Name = "FullTrust")]
        IntPtr KeyHookProc(int processMessage, IntPtr identifier, IntPtr hookStruct)
        {
            if (processMessage < 0)
                return NativeMethods.CallNextHookEx(kHook, processMessage, identifier, hookStruct);
            KeyboardInput KeyHookStruct = (KeyboardInput)Marshal.PtrToStructure(hookStruct, typeof(KeyboardInput));
            Keys key = HookVK2Keys(KeyHookStruct.VirtualKey);
            if (key==UnHookKey)
            {
                UnHookEvents();
                return NativeMethods.CallNextHookEx(kHook, processMessage, identifier, hookStruct);
            }
            if (key != Keys.None)
            {
                switch ((KeyboardWindowMessage)identifier)
                {
                    case KeyboardWindowMessage.KeyDown:
                        keyState[(int)key] = true;
                        if (KeyDown != null)
                            KeyDown(key);
                        break;
                    case KeyboardWindowMessage.KeyUp:
                        keyState[(int)key] = false;
                        if (KeyUp != null)
                            KeyUp(key);
                        break;
                    case KeyboardWindowMessage.SystemKeyDown:
                        keyState[(int)key] = true;
                        if (KeyDown != null)
                            KeyDown(key);
                        break;
                    case KeyboardWindowMessage.SystemKeyUp:
                        keyState[(int)key] = false;
                        if (KeyUp != null)
                            KeyUp(key);
                        break;
                }
            }
            return NativeMethods.CallNextHookEx(kHook, processMessage, identifier, hookStruct);
        }
       /// <summary>Tells if key is currently pressed</summary>
       /// <param name="key">key to check</param>
       /// <returns>true if key is pressed</returns>
        public bool IsKeyDown(Keys key)
        { 
            return keyState[(int)key];
        }

    }
}
