﻿using System;
using System.Runtime.InteropServices;
namespace Chip8
{
    class NativeMethods
    {
        public enum HookType
        {
            CbtHook = 5,
            DebugHook = 9,
            ForegroundIdleHook = 11,
            GetMessageHook = 3,
            JournalPlayHook = 1,
            JournalRecordHook = 0,
            KeyboardHook = 2,
            LowLevelKeyboardHook = 13,
            LowLevelMouseHook = 14,
            MessageFilterHook = -1,
            MouseHook = 7,
            ShellHook = 10,
            SystemMessageFilterHook = 6,
            WindowProcedureHook = 4,
            WindowProcedureReturnHook = 0x15
        }
        public delegate IntPtr HookProc(int processMessage, IntPtr identifier, IntPtr hookStruct);
        #region DllImport
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
        public static extern IntPtr LoadLibrary(string fileName);
        [DllImport("user32.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern IntPtr CallNextHookEx(IntPtr hookId, int code, IntPtr value1, IntPtr value2);
        [DllImport("user32.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Auto)]
        public static extern IntPtr SetWindowsHookEx(HookType hookType, HookProc hookProc, IntPtr moduleHandle, int threadId);
        [DllImport("user32.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Auto)]
        public static extern bool UnhookWindowsHookEx(IntPtr hookId);
        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern bool FreeLibrary([In] IntPtr module);
        #endregion
    }
}
