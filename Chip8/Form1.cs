﻿using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;


namespace Chip8
{
    public partial class Form1 : Form
    {
        Stopwatch cycleTimer = new Stopwatch();
        readonly double CYCLES_PER_SECOND = 500D;  //could not find stats on chip cycle times
        readonly double _microSecPerTick = 1000000D / System.Diagnostics.Stopwatch.Frequency;
        EmuChip8 chip8;
        KeyboardHook keyboardHook;
        bool stop = false;
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            keyboardHook = new KeyboardHook();
            x200ToolStripMenuItem.Checked = true;
        }

        private void RunRom(string filename)
        {
            chip8 = new EmuChip8();                 //cheap reset
            chip8.loadRom(filename);
            Bitmap bitmap = new Bitmap(EmuChip8.screenwidth, EmuChip8.screenheight);
            UnsafeBitmap bmp = new UnsafeBitmap(bitmap);
            BitArray b;
            stop = false;
            cycleTimer.Start();
            int cycles_to_catch_up = 0;
            TimeSpan elapsedSpan;
            double TimeRemainder = 0;
            while (!stop)
            {
                Application.DoEvents();
                elapsedSpan = new TimeSpan(cycleTimer.ElapsedTicks);
                cycles_to_catch_up = (int)(CYCLES_PER_SECOND * (elapsedSpan.TotalSeconds+TimeRemainder));
                TimeRemainder = elapsedSpan.TotalSeconds - (int)elapsedSpan.TotalSeconds;
                int cycles = 0;

                while (cycles_to_catch_up > cycles)
                {
                    cycles += 1;
                    chip8.emulateCycle();
                    if (chip8.drawFlag)
                    {
                        b = new BitArray(chip8.gfx);
                        bmp.LockBitmap();
                        bmp.DrawChip8Gfx(chip8.gfx, EmuChip8.screenwidth, EmuChip8.screenheight);
                        bmp.UnlockBitmap();
                        screen.Image = bmp.bitmap;
                    }
                }

                if (cycles > 0)
                    cycleTimer.Restart();
            }
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                stop = true;
                if (keyboardHook.IsHooked)
                    keyboardHook.UnHookEvents();
                if (!keyboardHook.IsHooked)
                    Console.WriteLine("UnHooked");
                chip8.Dispose();
                keyboardHook.Dispose();
            }
            catch (Exception)
            {
            }
        }

        private void loadRomToolStripMenuItem_Click(object sender, EventArgs e)
        {
            stop = true;
            ofd.InitialDirectory = Application.StartupPath + @"\Roms";
            if(ofd.ShowDialog()==DialogResult.OK)
            {
                RunRom(ofd.FileName); 
            }
        }

        private void x200ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EmuChip8.memoffset = 0x200;
            x200ToolStripMenuItem.Checked = true;
            x600ToolStripMenuItem.Checked = false;
        }

        private void x600ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EmuChip8.memoffset = 0x600;
            x600ToolStripMenuItem.Checked = true;
            x200ToolStripMenuItem.Checked = false;
        }
    }
}
