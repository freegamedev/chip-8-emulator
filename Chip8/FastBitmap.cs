﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;

namespace Chip8
{
    class FastBitmap
    {
        public Bitmap ProcessUsingLockbitsAndUnsafeAndTPL(Bitmap ProcessedBitmap)
        {
            unsafe
            {
                BitmapData bitmapData = ProcessedBitmap.LockBits(new Rectangle(0, 0, ProcessedBitmap.Width, ProcessedBitmap.Height), ImageLockMode.ReadWrite, ProcessedBitmap.PixelFormat);

                int BytesPerPixel = Bitmap.GetPixelFormatSize(ProcessedBitmap.PixelFormat) / 8;
                int HeightInPixels = bitmapData.Height;
                int WidthInBytes = bitmapData.Width * BytesPerPixel;
                byte* PtrFirstPixel = (byte*)bitmapData.Scan0;

                Parallel.For(0, HeightInPixels, y =>
                {
                    int BlueMagnitudeToAdd = 10;
                    int GreenMagnitudeToAdd = 0;
                    int RedMagnitudeToAdd = 255;
                    byte* CurrentLine = PtrFirstPixel + (y * bitmapData.Stride);
                    for (int x = 0; x < WidthInBytes; x = x + BytesPerPixel)
                    {
                        int OldBlue = CurrentLine[x];
                        int OldGreen = CurrentLine[x + 1];
                        int OldRed = CurrentLine[x + 2];
                        // Transform blue and clip to 255:
                        CurrentLine[x] = (byte)((OldBlue + BlueMagnitudeToAdd > 255) ? 255 : OldBlue + BlueMagnitudeToAdd);
                        // Transform green and clip to 255:
                        CurrentLine[x + 1] = (byte)((OldGreen + GreenMagnitudeToAdd > 255) ? 255 : OldGreen + GreenMagnitudeToAdd);
                        // Transform red and clip to 255:
                        CurrentLine[x + 2] = (byte)((OldRed + RedMagnitudeToAdd > 255) ? 255 : OldRed + RedMagnitudeToAdd);
                    }
                });
                ProcessedBitmap.UnlockBits(bitmapData);
            }
            return ProcessedBitmap;
        }
        public Bitmap Bytes2Bitmap(Bitmap bmp, byte[] b)
        {
            unsafe
            {
                BitmapData bitmapData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, bmp.PixelFormat);

                int BytesPerPixel = Bitmap.GetPixelFormatSize(bmp.PixelFormat) / 8;
                int HeightInPixels = bitmapData.Height;
                int WidthInBytes = bitmapData.Width * BytesPerPixel;
                byte* PtrFirstPixel = (byte*)bitmapData.Scan0;

                Parallel.For(0, HeightInPixels, y =>
                {
                    byte* CurrentLine = PtrFirstPixel + (y * bitmapData.Stride);
                    for (int x = 0; x < WidthInBytes; x += BytesPerPixel)
                    {
                        CurrentLine[x] = b[x];
                        CurrentLine[x + 1] = b[x];
                        CurrentLine[x + 2] = b[x];
                    }
                });
                bmp.UnlockBits(bitmapData);
            }
            return bmp;
        }
    }
}
