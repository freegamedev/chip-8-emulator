﻿    using System;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Runtime.InteropServices;
    using System.Collections;
    using System.Threading.Tasks;

public sealed class UnsafeBitmap : IDisposable
{
    [StructLayout(LayoutKind.Sequential)]
    public struct PixelData
    {
        public byte blue;
        public byte green;
        public byte red;
        public static bool operator ==(PixelData left, PixelData right)
        {
            if (left.blue == right.blue)
            {
                if (left.green == right.green)
                {
                    if (left.red == right.red)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public static bool operator !=(PixelData left, PixelData right)
        {
            if (left.blue == right.blue)
            {
                if (left.green == right.green)
                {
                    if (left.red == right.red)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        public static bool operator ==(PixelData left, Color right)
        {
            if (left.blue == right.B)
            {
                if (left.green == right.G)
                {
                    if (left.red == right.R)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public static bool operator !=(PixelData left, Color right)
        {
            if (left.blue == right.B)
            {
                if (left.green == right.G)
                {
                    if (left.red == right.R)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public override bool Equals(System.Object obj){return false;}   //garbage just to kill warnings
        public override int GetHashCode(){return this.GetHashCode(); }  //garbage just to kill warnings
    }
    #region Variables
    public Bitmap bitmap;
    private BitmapData bitmapData;
    private unsafe byte* pBase;
    /// <summary>stores with of bitmap</summary>
    private int width;
    #endregion
    #region Constructor / destructor

    public unsafe UnsafeBitmap(Bitmap bitmap)
    {
        this.pBase = null;
        this.bitmap = new Bitmap(bitmap);
    }

    public unsafe UnsafeBitmap(int width, int height)
    {
        this.pBase = null;
        this.bitmap = new Bitmap(width, height, PixelFormat.Format24bppRgb);
    }

    public void Dispose()
    {
        try { this.UnlockBitmap(); }
        catch (Exception) { }
        try { if (this.bitmap != null) this.bitmap.Dispose(); }
        catch (Exception) { }
    }
    #endregion
    #region Functions
    /// <summary>copy's a byte array binary to bitmap </summary>
    /// <param name="b">byte array</param>
    /// <param name="width">width of image in byte array</param>
    /// <param name="height">height of image in byte array</param>
    public unsafe void binary2bmp(byte[] b, int width, int height)
    {
        PixelData black = new PixelData
        {
            red = 0,
            green = 0,
            blue = 0
        };
        PixelData white = new PixelData
        {
            red = 0xff,
            green = 0xff,
            blue = 0xff
        };
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (b[(y * height) + x] == 0)
                {
                    *(this.PixelAt(x, y)) = black;
                }
                else
                {
                    *(this.PixelAt(x, y)) = white;
                }
            }
        }
    }
    public unsafe bool IsMatch(int x, int y, Color matchColor, double colorDistance)
    {
        PixelData inputColor = *(this.PixelAt(x, y));
        int r = matchColor.R - inputColor.red;
        int g = matchColor.G - inputColor.green;
        int b = matchColor.B - inputColor.blue;
        r *= r;
        g *= g;
        b *= b;
        return (Math.Sqrt((double)((r + g) + b)) < colorDistance);
    }
    public unsafe PixelData* PixelAt(int x, int y)
    {
        return (PixelData*)((this.pBase + (y * this.width)) + (x * sizeof(PixelData)));
    }
    public unsafe PixelData GetPixel(int x, int y)
    {
        return *(this.PixelAt(x, y));
    }
    public void SetPixel(int x, int y, Color color)
    {
        PixelData PD;
        PD.red = color.R;
        PD.green = color.G;
        PD.blue = color.B;
        SetPixel(x, y, PD);
    }
    public unsafe void SetPixel(int x, int y, PixelData color)
    {
        *(this.PixelAt(x, y)) = color;
    }
    public unsafe void DrawBitArray(BitArray b, int offset, int x, int y, int width, int height, bool littleEndian)
    {
        for (int h = 0; h < height; h++)
        {
            for (int w = 0; w < width; w++)
            {
                if (b[(h * width) + w + offset])
                {
                    if (littleEndian)
                    {
                        SetPixel(x + width - w, y + h, Color.Black);
                    }
                    else
                    {
                        SetPixel(x + w, y + h, Color.Black);
                    }
                }
                else
                {
                    if (littleEndian)
                    {
                        SetPixel(x + width - w, y + h, Color.White);
                    }
                    else
                    {
                        SetPixel(x + w, y + h, Color.White);
                    }
                }
            }
        }
    }
    public unsafe void DrawChip8Gfx(bool[] b, int width, int height)
    {
        Parallel.For(0, height, h =>
        {
            for (int w = 0; w < width; w++)
            {
                if (b[(h * width) + w])
                    SetPixel(w, h, Color.Blue);
                else
                    SetPixel(w, h, Color.Black);
            }
        });
    }
    /// <summary>Good and Fast</summary>
    public void line(int x, int y, int x2, int y2, PixelData color)
    {
        int w = x2 - x;
        int h = y2 - y;
        int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
        if (w < 0) dx1 = -1; else if (w > 0) dx1 = 1;
        if (h < 0) dy1 = -1; else if (h > 0) dy1 = 1;
        if (w < 0) dx2 = -1; else if (w > 0) dx2 = 1;
        int longest = Math.Abs(w);
        int shortest = Math.Abs(h);

        if (!(longest > shortest))
        {
            longest = Math.Abs(h);
            shortest = Math.Abs(w);
            if (h < 0) dy2 = -1; else if (h > 0) dy2 = 1;
            dx2 = 0;
        }
        int numerator = longest >> 1;
        for (int i = 0; i <= longest; i++)
        {
            SetPixel(x, y, color);
            numerator += shortest;
            if (!(numerator < longest))
            {
                numerator -= longest;
                x += dx1;
                y += dy1;
            }
            else
            {
                x += dx2;
                y += dy2;
            }
        }
    }
    public void Circle(int cx, int cy, int radius, Color color)
    {
        int error = -radius;
        int x = radius;
        int y = 0;
        PixelData PD;
        PD.red = color.R;
        PD.green = color.G;
        PD.blue = color.B;
        while (x >= y)
        {
            plot8points(cx, cy, x, y, PD);
            error += y;
            ++y;
            error += y;
            if (error >= 0)
            {
                error -= x;
                --x;
                error -= x;
            }
        }
    }
    void plot8points(int cx, int cy, int x, int y, PixelData PD)
    {
        plot4points(cx, cy, x, y, PD);
        if (x != y) plot4points(cx, cy, y, x, PD);
    }
    void plot4points(int cx, int cy, int x, int y, PixelData PD)
    {
        SetPixel(cx + x, cy + y, PD);
        if (x != 0) SetPixel(cx - x, cy + y, PD);
        if (y != 0) SetPixel(cx + x, cy - y, PD);
        if (x != 0 && y != 0) SetPixel(cx - x, cy - y, PD);
    }
    #endregion
    #region Lock/Unlock
    public unsafe void LockBitmap()
    {
        GraphicsUnit unit = GraphicsUnit.Pixel;
        RectangleF boundsF = this.bitmap.GetBounds(ref unit);
        Rectangle bounds = new Rectangle((int)boundsF.X, (int)boundsF.Y, (int)boundsF.Width, (int)boundsF.Height);
        this.width = ((int)boundsF.Width) * sizeof(PixelData);
        if ((this.width % 4) != 0)
        {
            this.width = 4 * ((this.width / 4) + 1);
        }
        this.bitmapData = this.bitmap.LockBits(bounds, ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
        this.pBase = (byte*)this.bitmapData.Scan0.ToPointer();
    }
    public unsafe void UnlockBitmap()
    {
        try
        {
            if (this.bitmapData != null)
            {
                this.bitmap.UnlockBits(this.bitmapData);
            }
            if (this.bitmapData != null)
            {
                this.bitmapData = null;
            }
            if (this.pBase != null)
            {
                this.pBase = null;
            }
        }
        catch (Exception)
        {
            //ErrorHandler.Show("UnsafeBitmap.UnlockBitmap : " + ex.ToString(), "UnsafeBitmap.UnlockBitmap Error");
        }
    }
    #endregion
}