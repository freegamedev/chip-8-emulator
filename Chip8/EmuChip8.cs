﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;
using System.Windows.Forms;
using System.Diagnostics;
using System.Media;

namespace Chip8
{
    class EmuChip8:IDisposable
    {
        Stopwatch delayTimer = new Stopwatch();
        Stopwatch soundTimer = new Stopwatch();
        float _60HZ = (1.0f / 60.0f) * 1000.0f; //60HZ

        bool ShowOpCodesInConsole = false;
        bool keypress = false;
        Keys lastKeyPressed = Keys.None;
        public static int memoffset = 0x200;    //but some begin at 0x600
        public bool drawFlag;                   //set when draw opcode is called
        Random rnd = new Random();
        public static int screenwidth = 64;
        public static int screenheight = 32;
        #region create chip structure
        #region fontset
        public byte[] fontset = new byte[80]
        { 
          0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
          0x20, 0x60, 0x20, 0x20, 0x70, // 1
          0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
          0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
          0x90, 0x90, 0xF0, 0x10, 0x10, // 4
          0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
          0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
          0xF0, 0x10, 0x20, 0x40, 0x40, // 7
          0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
          0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
          0xF0, 0x90, 0xF0, 0x90, 0x90, // A
          0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
          0xF0, 0x80, 0x80, 0x80, 0xF0, // C
          0xE0, 0x90, 0x90, 0x90, 0xE0, // D
          0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
          0xF0, 0x80, 0xF0, 0x80, 0x80  // F
        };
        #endregion
        //The graphics system: 
        //The chip 8 has one instruction that draws sprite to the screen. 
        //Drawing is done in XOR mode and if a pixel is turned off as a result of drawing, the VF register is set. 
        //This is used for collision detection.

        //The systems memory map:
        //0x000-0x1FF - Chip 8 interpreter (contains font set in emu)
        //0x050-0x0A0 - Used for the built in 4x5 pixel font set (0-F)
        //0x200-0xFFF - Program ROM and work RAM

        /// <summary>The Chip 8 has 35 opcodes which are all two bytes long.</summary>
        ushort opcode = 0;
        /// <summary>The Chip 8 has 4K memory in total</summary>
        byte[] memory = new byte[4096];
        /// <summary>CPU registers: 15 8-bit general purpose registers named V0 to VE. VE = ‘carry flag’.</summary>
        byte[] V = new byte[16];
        /// <summary>Index register (value from 0x000 to 0xFFF)</summary>
        ushort I = 0;
        /// <summary>Program Counter (value from 0x000 to 0xFFF)</summary>
        public ushort pc = 0;
        /// <summary>The graphics are black and white and the screen has a total of 2048 pixels (64 x 32)</summary>
        public bool[] gfx = new bool[screenwidth * screenheight];
        /// <summary>timer register that counts at 60 Hz. When set above zero it will count down to zero.</summary>
        byte delay_timer = 0;
        /// <summary>timer register that counts at 60 Hz. When set above zero it will count down to zero. The system’s buzzer sounds whenever the sound timer reaches zero.</summary>
        byte sound_timer = 0;
        /// <summary>opcodes allow the program to jump to a certain address or call a subroutine. Need stack to keep track</summary>
        ushort[] stack = new ushort[16];
        /// <summary>stack pointer</summary>
        ushort sp;
        /// <summary> Chip 8 has a HEX based keypad (0x0-0xF),</summary>
        bool[] hexInput = new bool[16];
        #endregion
        public EmuChip8()
        {
            pc = (ushort)memoffset; // Program counter starts at 0x200
            opcode = 0;             // Reset current opcode	
            I = 0;                  // Reset index register
            sp = 0;                 // Reset stack pointer

            // Clear display	
            // Clear stack
            // Clear registers V0-VF
            // Clear memory

            // Load fontset
            for (int i = 0; i < 80; ++i)
                memory[i] = fontset[i];

            // Reset timers
            delay_timer = 0;
            delayTimer.Stop();
            sound_timer = 0;
            soundTimer.Stop();
            Hook();
        }
        ~EmuChip8()
        {
            UnHook();
        }
        public void loadRom(string filename)
        {
            try
            {
                using (FileStream fs = File.OpenRead(filename))
                {

                    byte[] buffer = new byte[fs.Length];
                    fs.Read(buffer, 0, buffer.Length);
                    for (int i = 0; i < buffer.Length; i++)
                    {
                        memory[i + memoffset] = buffer[i];
                    }
                }
            }
            catch { }
        }

        // ran at 1 MHz(One MHz represents one million cycles per second)
        public void emulateCycle()
        {
            drawFlag = false;
            // Fetch Opcode
            byte[] parseRaw = new byte[2] { memory[pc + 1], memory[pc] };
            opcode = BitConverter.ToUInt16(parseRaw, 0); 
            //opcode = (ushort)(memory[(int)pc] << 8 | memory[(int)pc + 1]);
            #region easy read variables
            int X = (opcode & 0x0F00) >> 8;             //4-bit register identifier
            int Y = (opcode & 0x00F0) >> 4;             //4-bit register identifier
            byte N = (byte)(opcode & 0x000F);           //4-bit constant
            byte NN = (byte)(opcode & 0x00FF);          //8-bit constant
            ushort NNN = (ushort)(opcode & 0x0FFF);     //address
            #endregion
            // Decode & Execute Opcode
            switch (opcode & 0xF000)
            {
                case 0x0000:
                    switch (opcode)
                    {
                        #region 00E0 cls Clears the screen
                        case 0x00E0:
                            if (ShowOpCodesInConsole) Console.WriteLine("0x00E0 cls");
                            for (int i = 0; i < gfx.Length; i++)
                                gfx[i] = false;
                            pc += 2;
                            break;
                        #endregion
                        #region 00EE rts Returns from a subroutine
                        case 0x00EE:
                            if (ShowOpCodesInConsole) Console.WriteLine("0x00EE rts");
                            --sp;                                   //Decrement stack position
                            pc = stack[sp];                         //Place stack position in program counter
                            pc += 2;                                //Increment position since we already did call
                            break;
                        #endregion
                        #region 0NNN SYS addr - Calls RCA 1802 program at address NNN. (ignored by modern interpreters)
                        default:
                            if (ShowOpCodesInConsole) Console.WriteLine("0x0NNN RCA:" + NNN.ToString());
                            //pc = NNN;
                            break;
                        #endregion
                    }
                    break;
                #region 1NNN jmp Jumps to address NNN
                case 0x1000:
                    if (ShowOpCodesInConsole) Console.WriteLine("1NNN jmp:" + NNN.ToString());
                    pc = NNN;                                   //Jump to address
                    break;
                #endregion
                #region 2NNN jsr calls subroutine at NNN
                case 0x2000:
                    if (ShowOpCodesInConsole) Console.WriteLine("2NNN jsr:" + NNN.ToString());
                    stack[sp] = pc;                             //Place return position in stack
                    ++sp;                                       //Increment stack position
                    pc = NNN;                                   //Jump to address
                    break;
                #endregion
                #region 3XNN skeq skips the next instruction if VX equals NN
                case 0x3000:
                    if (ShowOpCodesInConsole) Console.WriteLine("3XNN skeq:" + NN.ToString());
                    pc += V[X] == NN ? (ushort)4 : (ushort)2; //if vx=nn pc+=4 else pc+=2
                    break;
                #endregion
                #region 4XNN skne skips the next instruction if VX doesn't equal NN
                case 0x4000:
                    if (ShowOpCodesInConsole) Console.WriteLine("4XNN skne:" + NN.ToString());
                    pc += V[X] != NN ? (ushort)4 : (ushort)2; //if vx != NN pc+=4 else pc+=2
                    break;
                #endregion
                #region 5XY0 skeq skips the next instruction if VX equals VY
                case 0x5000:
                    if (ShowOpCodesInConsole) Console.WriteLine("5XY0 skeq");
                    pc += V[X] == V[Y] ? (ushort)4 : (ushort)2; //if vx=vy pc+=4 else pc+=2
                    break;
                #endregion
                #region 6XNN mov sets VX to NN
                case 0x6000:
                    if (ShowOpCodesInConsole) Console.WriteLine("0x6XNN mov:" + NN.ToString());
                    V[X] = NN;
                    pc += 2;
                    break;
                #endregion
                #region 7XNN add adds NN to VX
                case 0x7000:
                    if (ShowOpCodesInConsole) Console.WriteLine("0x7XNN add:" + NN.ToString());
                    V[X] += NN;
                    pc += 2;
                    break;
                #endregion
                case 0x8000:
                    switch (N)
                    {
                        #region 8XY0 mov sets VX to the value of VY
                        case 0x0000:
                            if (ShowOpCodesInConsole) Console.WriteLine("8XY0 mov");
                            V[X] = V[Y];
                            pc += 2;
                            break;
                        #endregion
                        #region 8XY1 or sets VX to VX or VY
                        case 0x0001:
                            if (ShowOpCodesInConsole) Console.WriteLine("8XY1 or");
                            V[X] |= V[Y];
                            pc += 2;
                            break;
                        #endregion
                        #region 8XY2 and sets VX to VX and VY
                        case 0x0002:
                            if (ShowOpCodesInConsole) Console.WriteLine("8XY2 and");
                            V[X] &= V[Y];
                            pc += 2;
                            break;
                        #endregion
                        #region 8XY3 xor sets VX to VX xor VY
                        case 0x0003:
                            if (ShowOpCodesInConsole) Console.WriteLine("8XY3 xor");
                            V[X] ^= V[Y];
                            pc += 2;
                            break;
                        #endregion
                        #region 8XY4 add adds VY to VX. VF is set to 1 when there's a carry, and to 0 when there isn't.
                        case 0x0004:
                            if (ShowOpCodesInConsole) Console.WriteLine("8XY4 add");
                            if (V[Y] > (Byte.MaxValue - V[X]))  //y > 255-x
                                V[0xF] = 1;                     //carry (0x000F = 1)
                            else
                                V[0xF] = 0;
                            V[X] += V[Y];
                            pc += 2;
                            break;
                        #endregion
                        #region 8XY5 sub VY is subtracted from VX. VF is set to 0 when there's a borrow, and 1 when there isn't
                        case 0x0005:
                            if (ShowOpCodesInConsole) Console.WriteLine("8XY5 sub");
                            if (V[X] < V[Y])                    //y > x (todo check)
                                V[0xF] = 0;                     //borrow (0x000F = 0)
                            else
                                V[0xF] = 1;
                            V[X] -= V[Y];
                            pc += 2;
                            break;
                        #endregion
                        #region 8XY6 shr shifts VX right by one. VF is set to the value of the least significant bit of VX before the shift
                        case 0x0006:
                            if (ShowOpCodesInConsole) Console.WriteLine("8XY6 shr");
                            V[0xF] = (byte)(V[X] & 0x1);
                            V[X] >>= 1;
                            pc += 2;
                            break;
                        #endregion
                        #region 8XY7 rsb sets VX to VY minus VX. VF is set to 0 when there's a borrow, and 1 when there isn't
                        case 0x0007:
                            if (ShowOpCodesInConsole) Console.WriteLine("8XY7 rsb");
                            if (V[Y] < V[X])                    //y < x (todo check)
                                V[0xF] = 0;                     //borrow (0x000F = 0)
                            else
                                V[0xF] = 1;
                            V[X] = (byte)(V[Y] - V[X]);
                            pc += 2; break;
                        #endregion
                        #region 8XYE shl shifts VX left by one. VF is set to the value of the most significant bit of VX before the shift
                        case 0x000E:
                            if (ShowOpCodesInConsole) Console.WriteLine("8XYE shl");
                            V[0xF] = (byte)((V[X] & 0x80) >> 7);  //0x80= 1000 0000
                            V[X] <<= 1;
                            pc += 2;
                            break;
                        #endregion
                        default:                        //Unknown opcode
                            Console.WriteLine("Unknown opcode: " + opcode.ToString("X"));
                            break;
                    }
                    break;
                #region 9XY0 skne skips the next instruction if VX doesn't equal VY
                case 0x9000:
                    if (ShowOpCodesInConsole) Console.WriteLine("9XY0 skne");
                    pc += V[X] != V[Y] ? (ushort)4 : (ushort)2;
                    break;
                #endregion
                #region ANNN mvi sets I to the address NNN
                case 0xA000:
                    if (ShowOpCodesInConsole) Console.WriteLine("ANNN mvi:" + NNN.ToString());
                    I = NNN;
                    pc += 2;
                    break;
                #endregion
                #region BNNN jmi jumps to the address NNN plus V0
                case 0xB000:
                    if (ShowOpCodesInConsole) Console.WriteLine("BNNN jmi:" + NNN.ToString());
                    pc = (ushort)(NNN + V[0]);
                    break;
                #endregion
                #region CXNN rand sets VX to a random number and NN
                case 0xC000:
                    if (ShowOpCodesInConsole) Console.WriteLine("CXNN rand:" + NN.ToString());
                    V[X] = (byte)(rnd.Next(0,byte.MaxValue) & NN);
                    pc += 2;
                    break;
                #endregion
                #region DXYN sprite Draws a sprite at coordinate (VX, VY) that has a width of 8 pixels and a height of N pixels. Each row of 8 pixels is read as bit-coded (with the most significant bit of each byte displayed on the left) starting from memory location I; I value doesn't change after the execution of this instruction. As described above, VF is set to 1 if any screen pixels are flipped from set to unset when the sprite is drawn, and to 0 if that doesn't happen
                case 0xD000:
                    if (ShowOpCodesInConsole) Console.WriteLine("DXYN sprite:" + N.ToString());
                    ushort height = N;                              //height
                    ushort spriteWidth = 8;                         //width of sprite image in memory
                    byte RowData = 0;                               //8 bits of 1 row of sprite data
                    int screenPixel = 0;                            //current pixel being edited
                    int x = 0;
                    int y = 0;
                    V[0xF] = 0;                                     //reset flipped flag
                    for (int h = 0; h < height; h++)                //row
                    {
                        RowData = memory[I + h];                    //get sprite row of pixels from memory
                        for (int w = 0; w < spriteWidth; w++)       //col
                        {                                           //0x80 = 1000 0000
                            if ((RowData & (0x80 >> w)) != 0)       //check 1 bit at a time, see if pixel and line bit are 1  
                            {
                                x = V[X] + w;
                                y = V[Y] + h;

                                //If the sprite is positioned so part of it is outside the coordinates of the display, it wraps around to the opposite side of the screen.
                                //if (x > screenwidth-1)
                                //    while (x > screenwidth-1)
                                //        x = x - screenwidth;
                                //if (y > screenheight-1)
                                //    while (y > screenheight-1)
                                //        y = y - screenheight;                                        

                                screenPixel = (x + y * screenwidth);
                                if (screenPixel<gfx.Length )
                                {                                    
                                if (gfx[screenPixel])               //if that pixel is set on screen
                                    V[0xF] = 1;                     //set collision flag
                                gfx[screenPixel] ^= true;           //exclusive-OR (gfx=gfx^1) gfx must = 0 to become 1    
                                }
                            }
                        }
                    }

                    drawFlag = true;
                    pc += 2;
                    break;
                #endregion
                case 0xE000:
                    switch (NN)
                    {
                        #region EX9E skpr skips the next instruction if the key stored in VX is pressed
                        case 0x009E:
                            if (ShowOpCodesInConsole) Console.WriteLine("EX9E skpr");
                            if (hexInput[V[X]])
                                pc += 2;
                            pc += 2;
                            break;
                        #endregion
                        #region EXA1 skup skips the next instruction if the key stored in VX isn't pressed
                        case 0x00A1:
                            if (ShowOpCodesInConsole) Console.WriteLine("EXA1 skup");
                            if (!hexInput[V[X]])
                                pc += 2;
                            pc += 2;
                            break;
                        #endregion
                        default:                        //Unknown opcode
                            Console.WriteLine("Unknown opcode: " + opcode.ToString("X"));
                            break;
                    }
                    break;
                case 0xF000:
                    switch (NN)
                    {
                        #region FX07 gdelay sets VX to the value of the delay timer
                        case 0x0007:
                            if (ShowOpCodesInConsole) Console.WriteLine("FX07 gdelay");
                            V[X] = delay_timer;
                            pc += 2;
                            break;
                        #endregion
                        #region FX0A key a key press is awaited, and then stored in VX
                        case 0x000A:
                            if (ShowOpCodesInConsole) Console.WriteLine("FX0A key");
                            keypress = false;
                            lastKeyPressed = Keys.None;
                            Console.WriteLine("waiting for key press");
                            while (!keypress)
                                Application.DoEvents();
                            #region store V[X] = keypress
                            switch (lastKeyPressed)
                            {
                                case Keys.D1:
                                    V[X] = 0x01;
                                    break;
                                case Keys.D2:
                                    V[X] = 0x02;
                                    break;
                                case Keys.D3:
                                    V[X] = 0x03;
                                    break;
                                case Keys.D4:
                                    V[X] = 0x0C;
                                    break;
                                case Keys.Q:
                                    V[X] = 0x04;
                                    break;
                                case Keys.W:
                                    V[X] = 0x05;
                                    break;
                                case Keys.E:
                                    V[X] = 0x06;
                                    break;
                                case Keys.R:
                                    V[X] = 0x0D;
                                    break;
                                case Keys.A:
                                    V[X] = 0x07;
                                    break;
                                case Keys.S:
                                    V[X] = 0x08;
                                    break;
                                case Keys.D:
                                    V[X] = 0x09;
                                    break;
                                case Keys.F:
                                    V[X] = 0x0E;
                                    break;
                                case Keys.Z:
                                    V[X] = 0x0A;
                                    break;
                                case Keys.X:
                                    V[X] = 0x00;
                                    break;
                                case Keys.C:
                                    V[X] = 0x0B;
                                    break;
                                case Keys.V:
                                    V[X] = 0x0F;
                                    break;
                                default:
                                    Console.WriteLine("Error reading input");
                                    break;
                            }
                            #endregion
                            pc += 2;
                            break;
                        #endregion
                        #region FX15 sdelay sets the delay timer to VX
                        case 0x0015:
                            if (ShowOpCodesInConsole) Console.WriteLine("FX15 sdelay");
                            delay_timer = V[X];
                            delayTimer.Restart(); //reset timer for first tick
                            pc += 2;
                            break;
                        #endregion
                        #region FX18 ssound sets the sound timer to VX
                        case 0x0018:
                            if (ShowOpCodesInConsole) Console.WriteLine("FX18 ssound");
                            sound_timer = V[X];
                            pc += 2;
                            break;
                        #endregion
                        #region FX1E adi adds VX to I
                        case 0x001E:
                            if (ShowOpCodesInConsole) Console.WriteLine("FX1E adi");
                            I += V[X];
                            pc += 2;
                            break;
                        #endregion
                        #region FX29 font sets I to the location of the sprite for the character in VX. Characters 0-F (in hexadecimal) are represented by a 4x5 font
                        case 0x0029:
                            if (ShowOpCodesInConsole) Console.WriteLine("FX29 font");
                            I = (ushort)(V[X] * 0x5);     //5 high
                            pc += 2;
                            break;
                        #endregion
                        #region FX33 bcd stores the Binary-coded decimal representation of VX, with the most significant of three digits at the address in I, the middle digit at I plus 1, and the least significant digit at I plus 2
                        case 0x0033:
                            if (ShowOpCodesInConsole) Console.WriteLine("FX33 bcd");
                            memory[I + 0] = (byte)((V[X] / 100));      //234 / 100 = 2
                            memory[I + 1] = (byte)((V[X] / 10) % 10);  //234 / 10  = 23  (23 % 10 = 3)
                            memory[I + 2] = (byte)((V[X] % 10));       //234 % 10  = 4
                            pc += 2;
                            break;
                        #endregion
                        #region FX55 str stores V0 to VX in memory starting at address I (I is incremented to point to the next location on. e.g. I = I + X + 1)
                        case 0x0055:
                            if (ShowOpCodesInConsole) Console.WriteLine("FX55 str");
                            for (int i = 0; i < X+1; i++)   //includes X
                                memory[I + i] = V[i];
                            I += (ushort)(X + 1);
                            pc += 2;
                            break;
                        #endregion
                        #region FX65 ldr fills V0 to VX with values from memory starting at address I
                        case 0x0065:
                            if (ShowOpCodesInConsole) Console.WriteLine("FX65 ldr");
                            for (int i = 0; i < X + 1; i++) //includes X
                                V[i] = memory[I + i];
                            pc += 2;
                            break;
                        #endregion
                        default:                        //Unknown opcode
                            Console.WriteLine("Unknown opcode: " + opcode.ToString("X"));
                            pc += 2;
                            break;
                    }
                    break;
                default:
                    Console.WriteLine("Unknown opcode: " + opcode.ToString("X"));
                    break;
            }

            // Update timers
            if (delay_timer > 0)
            {
                if (delayTimer.Elapsed.Milliseconds >= _60HZ)
                {
                    //Console.WriteLine(delayTimer.Elapsed.Milliseconds + " " + _60HZ);
                    delayTimer.Restart();
                    --delay_timer;
                }
            }
            else
            {
                delayTimer.Start();
            }
            if (sound_timer > 0)
            {

                if (soundTimer.Elapsed.Milliseconds >= _60HZ)
                {
                    soundTimer.Restart(); 
                    if (sound_timer != 0)
                        new SoundPlayer(Application.StartupPath + @"\blip.wav").Play();
                    --sound_timer;
                }
            }
            else
            {
                soundTimer.Start(); 
            }
        }
        #region Keyboard Hooking
        KeyboardHook keyboardHook;
        private void HookKeyDown(Keys key)
        {
            keypress = true;
            lastKeyPressed = key;
            switch (key)
            {
                case Keys.D1:
                    hexInput[0x01] = true;
                    break;
                case Keys.D2:
                    hexInput[0x02] = true;
                    break;
                case Keys.D3:
                    hexInput[0x03] = true;
                    break;
                case Keys.D4:
                    hexInput[0x0C] = true;
                    break;
                case Keys.Q:
                    hexInput[0x04] = true;
                    break;
                case Keys.W:
                    hexInput[0x05] = true;
                    break;
                case Keys.E:
                    hexInput[0x06] = true;
                    break;
                case Keys.R:
                    hexInput[0x0D] = true;
                    break;
                case Keys.A:
                    hexInput[0x07] = true;
                    break;
                case Keys.S:
                    hexInput[0x08] = true;
                    break;
                case Keys.D:
                    hexInput[0x09] = true;
                    break;
                case Keys.F:
                    hexInput[0x0E] = true;
                    break;
                case Keys.Z:
                    hexInput[0x0A] = true;
                    break;
                case Keys.X:
                    hexInput[0x00] = true;
                    break;
                case Keys.C:
                    hexInput[0x0B] = true;
                    break;
                case Keys.V:
                    hexInput[0x0F] = true;
                    break;
                default:
                    keypress = false;
                    lastKeyPressed = Keys.None;
                    break;
            }
        }
        private void HookKeyUp(Keys key)
        {
            keypress = false;
            lastKeyPressed = key;
            switch (key)
            {
                case Keys.D1:
                    hexInput[0x01] = false;
                    break;
                case Keys.D2:
                    hexInput[0x02] = false;
                    break;
                case Keys.D3:
                    hexInput[0x03] = false;
                    break;
                case Keys.D4:
                    hexInput[0x0C] = false;
                    break;
                case Keys.Q:
                    hexInput[0x04] = false;
                    break;
                case Keys.W:
                    hexInput[0x05] = false;
                    break;
                case Keys.E:
                    hexInput[0x06] = false;
                    break;
                case Keys.R:
                    hexInput[0x0D] = false;
                    break;
                case Keys.A:
                    hexInput[0x07] = false;
                    break;
                case Keys.S:
                    hexInput[0x08] = false;
                    break;
                case Keys.D:
                    hexInput[0x09] = false;
                    break;
                case Keys.F:
                    hexInput[0x0E] = false;
                    break;
                case Keys.Z:
                    hexInput[0x0A] = false;
                    break;
                case Keys.X:
                    hexInput[0x00] = false;
                    break;
                case Keys.C:
                    hexInput[0x0B] = false;
                    break;
                case Keys.V:
                    hexInput[0x0F] = false;
                    break;
                default:
                    keypress = false;
                    lastKeyPressed = Keys.None;
                    break;
            }
        }
        void Hook()
        {
            keyboardHook = new KeyboardHook();
            keyboardHook.KeyUp += new delKeyUp(HookKeyUp);
            keyboardHook.KeyDown += new delKeyDown(HookKeyDown);
            keyboardHook.HookEvents();
            if (keyboardHook.IsHooked)
            {
                Console.WriteLine("Keyboard Hooked");
            }
        }
        void UnHook()
        {
            keyboardHook.UnHookEvents();
            if (!keyboardHook.IsHooked)
            {
                Console.WriteLine("UnHooked");
            }
        }

        public void Dispose()
        {
            ((IDisposable)keyboardHook).Dispose();
        }
        #endregion
    }
}
