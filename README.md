Chip 8 Emulator

![Chip8.png](https://bitbucket.org/repo/e46jyz/images/4024939310-Chip8.png)

This is a Chip 8 emulator that I hope will be easy to understand for those who want to learn about emulators. It is written entirely in c# and does not have too many frills in order to keep the code fairly compact.

Usage is fairly straight forward. Just load the rom and use the q, w, e buttons to move and fire. The rom added has the credit right in the loading screen for the person who developed it. It is not mine.

You can download more roms online and run them as well. They keyboard mapping you may have to figure out for each rom though.

I made this a while ago and forget all of the resources on the net that I pulled from, but I do remember one was here. http://www.multigesture.net/articles/how-to-write-an-emulator-chip-8-interpreter/

I am always looking for people to program with. I am mostly working on game development at the moment. Using unity3d.

ps. To protect myself. The source code is provided as is, use at your own risk.